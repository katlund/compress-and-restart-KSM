# CRKSM_MatEQ

CRKSM_MatEQ is academic Matlab code implementing compress-and-restart polynomial Krylov subspace methods (KSMs) for solving Lyapunov and Sylvester matrix equations.  Analogous extended KSMs for matrix equations, without restarts, are also implemented.  A version of SKSM_cTri (https://zenodo.org/record/3252320#.XjGtSMhKg2x) is additionally included and adapted for comparisons with the restarted polynomial and extended KSMs.


## Installation
Follow the download options from the Git repository main page.

## Usage
For the tests used in the paper [citation below], see
* laplacian_2d_rand_lyap.m (Table 4.1, Figure 4.1, Figure 4.2 - left)
* laplacian_2d_rand_nonnorm_lyap.m (Table 4.2 - right)
* laplacian_2d_DecayRHS_lyap.m (Table 4.2)
* conv_diff_3d_sylv.m (Figure 4.3, Table 4.3, Table 4.4)


## Documentation
The core functions, restarted_ksm_mat_eq.m, extended_ksm_mat_eq.m, and
standard_ksm_lyap.m detail all variable and parameter options in their headers.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to
discuss what you would like to change.


## Citing
When using the code, please cite the following paper: D. Kressner, K. Lund, S. Massei, and D. Palitta. "Compress-and-restart block Krylov subspace methods for Sylvester matrix equations." Numerical Linear Algebra and its Applications, 28(1):e2339, 2021. https://doi.org/10.1002/nla.2339
