% CONV_DIFF_3D_SYLV computes the solution to the Sylvester equation
%
%                       A * X + X * B + C * D' = 0
%
% coming from the discretization of a 3D convection-diffusion equation via
% a restarted polynomial KSM, various versions extended KSM, and a standard
% KSM with two-pass Lanczos. This file serves as a basic example and
% starting point for using the RESTARTED_KSM_MAT_EQ routine.
%
% The methods are run via a for-loop with index 'met':
%   met == 1: restarted polyomial KSM
%   met == 2: extended KSM with inexact A and B solves (BGMRES)
%   met == 3: extended KSM with inexact A and B solves (BGMRES) and ILU
%       preconditioning
%   met == 4: extended KSM with backslash A and B solves
%   met == 5: extended KSM with precomputed LU factors

%%
addpath(genpath('../'));
addpath('matrices');

%% Defaults for all tests
max_restarts = 500;
memory_max = 264; % parameter for Table 4.2, 4.3 (first column) and Figure 4.3
%memory_max = 324; % parameter for Table 4.3 (last column)
tol_res = 1e-6;
tol_comp = tol_res*1e-4;
verbose = 0;

%% Load data matrices
% n = 10; r = 3; % good for debugging
n = 25; r = 3; % parameter for Table 4.2, 4.3 (first column) and Figure 4.3
%n = 80; r = 3; % parameter for Table 4.3 (last column)

filestr = sprintf('matrices/conv_diff_3d_sep_n%d_r%d.mat',n^3,r);
if ~exist(filestr,'file')
    cd matrices
    conv_diff_3d_sep(n,r,false); % computes A, B and C
    cd ..
end
load(filestr) % loads A, B, C, D, and Xtrue

mkdir(sprintf('results/%s',mfilename));
N = size(A,1);
r = size(C,2);
fstr = sprintf('results/%s/%s_n%d_r%d.txt',mfilename,mfilename,N,r);
fID = fopen(fstr,'w');

%% Solvers
for met = [1 2 3 4 5]
    % Initialize parameters
    param = [];
    param.memory_max = memory_max;
    param.norm = 'fro';
    param.tol_res = tol_res;
    param.tol_comp = tol_comp;
    param.verbose = verbose;
    
    % Switch between RPK and EK
    profile on
    switch met
        case 1
            fprintf(fID,'Restarted polynomial Krylov\n');
            method = 'rpk';                                                 % restarted polynomial Krylov
            param.max_restarts = max_restarts;
        
            % Solve
            tt = tic;
            param = restarted_ksm_mat_eq(A,B,C,D,param);
            total_time = toc(tt);
            
        case {2, 3}
            if n > 50 && met == 2
                warning('EK without preconditioning may take a long time to converge.\n');
            end
            fprintf(fID,'Extended Krylov (with inexact solves)\n');
            method = 'ek';                                                  % extended krylov

            % Parameters for BLOCK_SOLVE
            ek_paramA = [];
            ek_paramA.max_restarts = max_restarts;
            ek_paramA.solver = 'bgmres';                                    % options include bcg, bfom, and bgmres
            ek_paramA.tol_res = tol_res/100;                                % measure error per restart cycle
            ek_paramA.verbose = 0;
            
            ek_paramB = ek_paramA;

            % Preconditioner
            if met == 3
                method = 'ek_ilu';
                fprintf(fID,'and ILU preconditioning\n');
                [L, U] = ilu(A);
                ek_paramA.precond_left = L;                                  % L\(A*x) = L\b
                ek_paramA.precond_right = U;                                 % A*(U\y) = b, x = U\y

                [L, U] = ilu(B);
                ek_paramB.precond_left = L;                                  % L\(A*x) = L\b
                ek_paramB.precond_right = U;                                 % A*(U\y) = b, x = U\y
            end
            
            Astruct = struct_make(A,ek_paramA);
            Bstruct = struct_make(B,ek_paramB);
            
            % Solve
            tt = tic;
            param = extended_ksm_mat_eq(Astruct,Bstruct,C,D,param);
            total_time = toc(tt);
            
        case 4
            fprintf(fID,'Extended Krylov (with only backslash)\n');
            method = 'ek_exact_backslash';
            
            Astruct = struct(...
                'solve', @(x) A\x,...
                'multiply', @(x) A * x,...
                'solver', 'backslash');

            Astruct.trans = struct(...
                'solve', @(x) A'\x,...
                'multiply', @(x) A' * x,...
                'solver', 'backslash');
            
            Bstruct = struct(...
                'solve', @(x) B\x,...
                'multiply', @(x) B * x,...
                'solver', 'backslash');

            Bstruct.trans = struct(...
                'solve', @(x) B'\x,...
                'multiply', @(x) B' * x,...
                'solver', 'backslash');
            
            % Solve
            tt = tic;
            param = extended_ksm_mat_eq(Astruct,Bstruct,C,D,param);
            total_time = toc(tt);
            
        case 5
            fprintf(fID,'Extended Krylov (with precomputed LU factors)\n');
            method = 'ek_exact_precompLU';
            
            % Solve
            tt = tic;
            param = extended_ksm_mat_eq(A,B,C,D,param);
            total_time = toc(tt);
    end
    
    %% Close profiling
    profstr = sprintf('results/%s/%s%d',mfilename,method,floor(now));
    profsave(profile('info'),profstr);
    profile off
    
    %% Save to output file
    print_flag(param.flag, fID);
    fprintf(fID,'Total time to solution: %3.2f s\n', total_time);
    fprintf(fID,'Final residual: %d\n',...
        compute_true_res(A,B,C,D,param));
    fprintf(fID,'Acount: %d | Bcount: %d\n',...
        param.Acount(1), param.Acount(2));
    fprintf(fID,'matvecs for A: %d | B: %d\n',...
        param.matvecs(1), param.matvecs(2));
    fprintf(fID,'Efficiency for A: %d | for B: %d \n',...
        floor(param.matvecs./param.Acount));
    fprintf(fID,'Number of iterations: %d\n',length(param.residual_norms));
    
    %% Plot and save output
    m = param.memory_max;
    savestr = sprintf('results/%s/%s_n%d_r%d_m%d',mfilename,method,N,r,m);
    save(savestr)

    clf
    if met == 1
        fprintf(fID,'Number of restart cycles (including the first): %d\n',...
            param.num_restarts);
        
        rank_str = sprintf('%s_ranks.dat',savestr);
        len = length(param.residual_ranks);                                 % based on the number of cycles for RPK
        dlmwrite(rank_str,...
            [(1:len)' param.residual_ranks' param.solution_ranks(1:len)'],...
            '\t'); 
        
        cycle_markers = zeros(1,param.num_restarts);                        % marks the iteration index at which a restart occurs
        cycle_markers(1) = 0;
        for i = 2:param.num_restarts
            cycle_markers(i) = cycle_markers(i-1) + param.it_per_cycle(i-1);
        end
        cycle_markers(1) = 1;                                               % have to adjust for plotting purposes
        cycle_str = sprintf('%s_cycle_markers.dat',savestr);
        dlmwrite(cycle_str,...
            [cycle_markers' param.residual_norms(cycle_markers)'],...
            '\t');

        subplot(1,3,1)
        plot(1:length(param.residual_ranks), param.residual_ranks, '.-')
        grid on
        title('Rank of the residual')
        xlabel('Cycle')

        subplot(1,3,2)
        plot(1:length(param.solution_ranks), param.solution_ranks, '.-')
        grid on
        title('Rank of the approximate solution')
        xlabel('Cycle')

        subplot(1,3,3)
    end
    semilogy(1:length(param.residual_norms), param.residual_norms, '-')
    grid on
    title('Sequence of residual norms')
    xlabel('Iteration')

    savefig(savestr)

    %% Format .dat plots for paper
    res_norm_str = sprintf('%s_residual_norms.dat',savestr);
    len = length(param.residual_norms);                                     % number of total iterations
    dlmwrite(res_norm_str,  [(1:len)' param.residual_norms(1:len)'], '\t'); 
    
    fprintf(fID,'\n');
end
fclose(fID);
close all
open(fstr)
