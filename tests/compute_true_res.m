function R = compute_true_res(A, B, C, D, param)
% A subroutine for computing the true residual in test scripts

%%
if isempty(B)
    % Lyapunov
    fact = [A*param.Xleft, param.Xleft, C];
    Theta = eye(size(fact, 2));
    Theta(1:2*size(param.Xleft, 2), 1:2*size(param.Xleft,2)) = ...
        [zeros(size(param.Xleft, 2)), param.S;...
        param.S, zeros(size(param.Xleft, 2))];
    [~, R] = qr(full(fact), 0);
    switch param.norm
            case 'fro'
                R = norm(full(R * Theta * R'), param.norm) / norm(C'*C, param.norm);   
            case {2, '2'}
                [~,RC]=qr(C,0);
                R = norm(full(R * Theta * R'), param.norm) / norm(RC*RC', param.norm);
    end    
    
else
    % Sylvester
    fact_left = [A*param.Xleft, param.Xleft, C];
    fact_right = [param.Xright, B'*param.Xright, D];
    [~, R_left] = qr(full(fact_left), 0);
    [~, R_right] = qr(full(fact_right),0);
    switch param.norm
            case 'fro'
                R = norm(full(R_left * R_right'), param.norm) / sqrt(trace((C'*C)*(D'*D)));   
            case {2, '2'}
                [~,RC]=qr(C,0);
                [~,RD]=qr(D,0);
                R = norm(full(R_left * R_right'), param.norm) / norm(RC*RD', param.norm);
    end    
end
end