function [A, C, Xtrue] = laplacian_2d_nonnorm(n,r,exact_sol)
% LAPLACIAN_2D creates a Laplacian matrix A of dimension n^2 and random
% data C of rank r for setting up the Lyapunov equation A X + X A' + C C' =
% 0.  Data are saved in an eponynomous .mat file with n and r specified. By
% default, n^2 = 1024 and r = 2.

%%
if nargin == 0
    n = 32;
    r = 2;
    exact_sol = true;
elseif nargin == 1
    r = 2;
    exact_sol = true;
elseif nargin == 2
    exact_sol = true;
end

e = ones(n,1);
A = spdiags([e -2*e e], -1:1, n, n);
I = speye(n);
A = kron(A, I) + kron(I, A);

rng('default');
C = randn(n^2, r);
%C = C / sqrt(norm(C'*C,'fro'));

if exact_sol
    Xtrue = lyap(A,C*C');
end

savestr = sprintf('%s_n%d_r%d',mfilename,n^2,r);
save(savestr)
fprintf('A, C, and Xtrue saved as ../matrices/%s\n',savestr);
end