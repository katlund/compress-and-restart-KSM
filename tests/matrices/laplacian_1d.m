function [A, C, Xtrue] = laplacian_1d(n,r,exact_sol)
% LAPLACIAN_1D creates a matrix A of dimension n and random data C of rank
% r for setting up the Lyapunov equation A X + X A' + C C' = 0.  Data are
% saved in an eponynomous .mat file with n and r specified. By default, n =
% 1024 and r = 2.

%%
if nargin == 0
    n = 1024;
    r = 2;
    exact_sol = true;
elseif nargin == 1
    r = 2;
    exact_sol = true;
elseif nargin == 2
    exact_sol = true;
end

% 1D Laplacian
A = 2*eye(n) - diag(ones(n-1, 1),-1) - diag(ones(n-1,1),1);
A = -A;

% Random RHS
C = randn(n,r);
C = C/norm(C);

if exact_sol
    Xtrue = lyap(A,C*C');
end

savestr = sprintf('%s_n%d_r%d',mfilename,n,r);
save(savestr)
fprintf('A, C, and Xtrue saved as ../matrices/%s\n',savestr);
end