function L = fourdrhs(n,p,tol)
   % generates low-rank approximation of n^2-times-n^2 matrix representing
   % discretization of exp( ( x1^p + x2^p + x3^p + x4^p )^1/p ) on the
   % hypercube [-1,1]^4. p should be an even integer.
   % The approximation is roughly bounded by tol
   
   % Build full matrix just for reference
    x = (1:n)/(n+1); % * 2 - 1;
%    if n <= 40,
%        A = zeros(n^2,n^2);
%        for j = 1:n^2,
%            A(:,j) = col(j);
%        end
%    end
   
   % Run ACA
   d = fundiag';
   L = zeros(n^2,0);
   while max(d) > tol,
       [m,j] = max(d);
       l = col(j) - L*L(j,:)';
       l = l / sqrt(m);
       
       
       d = d - l.*l;
       L = [L,l];
   end
      
%    if n <= 40,
%       disp(sprintf('Error = %g',norm(A-L*L'))),
%    end
%    
   
   

 function c = col(j)
     % returns column j of the matrix
     ind4 = mod(j-1,n)+1;
     ind3 = floor( (j-1)/ n ) + 1;
     ind1 = kron(1:n,ones(1,n))';
     ind2 = kron(ones(1,n),1:n)';
     c = exp( - ( ( x(ind1) + x(ind2) ).^p  + ( x(ind3) + x(ind4) ).^p ).^(1/p ) )';
     % c = exp( ( x(ind1).^p + x(ind2).^p  + x(ind3).^p + x(ind4).^p ).^(1/p ) );
 end

 function d = fundiag
     % generate diagonal of matrix
     ind1 = kron(1:n,ones(1,n))';
     ind2 = kron(ones(1,n),1:n)';
     d = exp( - ( ( x(ind1) + x(ind2) ).^p  + ( x(ind1) + x(ind2) ).^p ).^(1/p ) );
 end
   
end

