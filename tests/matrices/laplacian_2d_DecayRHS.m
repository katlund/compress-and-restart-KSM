n = 100; r = 3;
A = -(n+1)^2 * gallery('poisson',n);

% Generate the rhs
tol = 10^(-12);
p = 2;
C = fourdrhs(n,p,tol);
C = C / sqrt(norm(C'*C,'fro'));

save(mfilename, 'A', 'C');