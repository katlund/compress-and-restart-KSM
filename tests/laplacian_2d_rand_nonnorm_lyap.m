% LAPLACIAN_2D_LYAP computes the solution to the Lyapunov equation
%
%                       A * X + X * A' + C * C' = 0,
%
% where A is the 2D Laplacian and C is a random matrix (NON normalized), via a
% restarted polynomial KSM, various versions extended KSM, and a standard
% KSM with two-pass Lanczos. This file serves as a basic example and
% starting point for using the RESTARTED_KSM_MAT_EQ routine.
%
% The methods are run via a for-loop with index 'met':
%   met == 1: restarted polyomial KSM
%   met == 2: extended KSM with inexact A solves (BCG)
%   met == 3: extended KSM with inexact A solves (BCG) and ILU
%       preconditioning
%   met == 4: extended KSM with precomputed Cholesky A solves
%   met == 5: standard KSM with two-pass Lanczos

%%
addpath(genpath('../'));
addpath('matrices');

%% Defaults for all tests
max_restarts = 500;                                                         % maximum number of restart cycles for RPK
memory_max = 96;                                                            % maximum number of column vectors a method can store at once
tol_res = 1e-6;                                                             % target residual tolerance
tol_comp = tol_res*1e-4;                                                    % compression tolerance
verbose = 1;                                                                % whether to print intermediate outputs to screen

%% Load data matrices
% n = 25; r = 3; % good for quick checks
n = 100; r = 3; % good for paper
filestr = sprintf('matrices/laplacian_2d_nonnorm_n%d_r%d.mat',n^2,r);
if ~exist(filestr,'file')
    cd matrices
    if n > 40
        laplacian_2d_nonnorm(n,r,false);                                            % computes A and C
    elseif n <= 40
        laplacian_2d_nonnorm(n,r,true);                                             % computes A, C, and Xtrue
    end
    cd ..
end
load(filestr);                                                              % loads A, C, and Xtrue

%% Prepare folders and strings for outputs
mkdir(sprintf('results/%s',mfilename));
N = size(A,1);
r = size(C,2);
fstr = sprintf('results/%s/%s_n%d_r%d.txt',mfilename,mfilename,N,r);
fID = fopen(fstr,'w');

%% Solvers
for met = 1:1
    % Initialize parameters
    param = [];
    param.memory_max = memory_max;
    param.norm = 'fro';                                                     % necessary for matching Davide's routine
    param.tol_res = tol_res;
    param.tol_comp = tol_comp;
    param.verbose = verbose;
        
    % Switch between RPK and EK
    profile on
    switch met
        case 1
            fprintf(fID,'Restarted polynomial Krylov\n');
            method = 'rpk';     % restarted polynomial Krylov
            param.max_restarts = max_restarts;

            % Solve
            tt = tic;
            param = restarted_ksm_mat_eq(A,[],C,[],param);
            total_time = toc(tt);
            
        case {2,3}
            fprintf(fID,'Extended Krylov (without restarts)\n');
            method = 'ek';      % extended krylov

            % Parameters for BLOCK_SOLVE
            ek_param = [];
            ek_param.solver = 'bcg';                                        % options include bcg, bfom, and bgmres
            ek_param.bcg_variant = 'bcgrq';                                 % options include 'bcg', 'bcga', 'bcgadq', and 'bcgrq'
            ek_param.hermitian = true;                                      % specification for struct_make
            ek_param.max_iterations = 10000;
            ek_param.max_restarts = max_restarts;
            ek_param.verbose = 0;

            % Preconditioner
            if met == 3
                method = 'ek_ilu';
                fprintf(fID,'and ILU preconditioning\n');
                [L, U] = ilu(A);
                I = speye(size(A));
                ek_param.precond_left = L;                                  % L\(A*x) = L\b
                ek_param.precond_right = U;                                 % A*(U\y) = b, x = U\y
             end
            
            Astruct = struct_make(A, ek_param);
            
            % Solve
            tt = tic;
            param = extended_ksm_mat_eq(Astruct,[],C,[],param);
            total_time = toc(tt);
    
        case 4
            fprintf(fID,'Extended Krylov (with precomputed Cholesky)\n');
            method = 'ek_exact_precomp_chol';
            
            % Solve
            tt = tic;
            param = extended_ksm_mat_eq(A,[],C,[],param);
            total_time = toc(tt);
            
        case 5
            fprintf(fID,'Standard KSM with Two-Pass Lanczos\n');
            method = 'sksm';
            param.max_iterations = max_restarts;                            % no real memory restrictions; just a fail-safe
            
            % Solve
            tt = tic;
            param = standard_ksm_lyap(A,C,param);
            total_time = toc(tt);
            
            param.S = speye(size(param.Xleft,2));
    end
    
    %% Close profiling
    profstr = sprintf('results/%s/%s%d',mfilename,method,floor(now));
    profsave(profile('info'),profstr);
    profile off
    
    %% Save to output file
    print_flag(param.flag, fID);
    fprintf(fID,'Total time to solution: %3.2f s\n', total_time);
    fprintf(fID,'Final residual: %d\n',...
        compute_true_res(A,[],C,[],param));
    fprintf(fID,'Acount: %d\n',sum(param.Acount));
    fprintf(fID,'matvecs: %d\n',sum(param.matvecs));
    fprintf(fID,'Efficiency (matvecs per A-call): %d\n',...
        floor(sum(param.matvecs)/sum(param.Acount)));
    fprintf(fID,'Number of iterations: %d\n',length(param.residual_norms));
    
    %% Plot and save output
    m = param.memory_max;
    savestr = sprintf('results/%s/%s_n%d_r%d_m%d',mfilename,method,N,r,m);
    save(savestr)

    clf
    if met == 1
        fprintf(fID,'Number of restart cycles (including the first): %d\n',...
            param.num_restarts);
        
        rank_str = sprintf('%s_ranks.dat',savestr);
        len = length(param.residual_ranks);                                 % based on the number of cycles for RPK
        dlmwrite(rank_str,...
            [(1:len)' param.residual_ranks' param.solution_ranks(1:len)'],...
            '\t'); 
        
        cycle_markers = zeros(1,param.num_restarts);                        % marks the iteration index at which a restart occurs
        cycle_markers(1) = 0;
        for i = 2:param.num_restarts
            cycle_markers(i) = cycle_markers(i-1) + param.it_per_cycle(i-1);
        end
        cycle_markers(1) = 1;                                               % have to adjust for plotting purposes
        cycle_str = sprintf('%s_cycle_markers.dat',savestr);
        dlmwrite(cycle_str,...
            [cycle_markers' param.residual_norms(cycle_markers)'],...
            '\t');
        
        % Save some eigenvalue data... param.min_core_eig?

        subplot(1,3,1)
        plot(1:length(param.residual_ranks), param.residual_ranks, '.-')
        grid on
        title('Rank of the residual')
        xlabel('Cycle')

        subplot(1,3,2)
        plot(1:length(param.solution_ranks), param.solution_ranks, '.-')
        grid on
        title('Rank of the approximate solution')
        xlabel('Cycle')

        subplot(1,3,3)
    end
    semilogy(1:length(param.residual_norms), param.residual_norms, '-')
    grid on
    title('Sequence of residual norms')
    xlabel('Iteration')

    savefig(savestr)

    %% Format .dat plots for paper
    res_norm_str = sprintf('%s_residual_norms.dat',savestr);
    len = length(param.residual_norms);                                     % number of total iterations
    dlmwrite(res_norm_str,  [(1:len)' param.residual_norms(1:len)'], '\t'); 
    
    fprintf(fID,'\n');
end
fclose(fID);
close all
open(fstr)
